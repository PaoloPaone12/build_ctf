import subprocess
import os
import shutil

output_directory = "lab_ctf"

os.makedirs(output_directory, exist_ok=True)
print(output_directory + " directoty created")


script_names = [
    "build_lab.py",
    "clone_services.py",
    "generate_pcn_startup.py",
    "generate_db_dir.py",
    "generate_dbstartup.py",
    "generate_gm_dir_startup.py",
    "generate_gm_startup.py",
    "generate_mr_startup.py",
    "generate_vm_startup.py",
    "generate_vmdir.py",
    "services.py"
]

for script_name in script_names:
	subprocess.run(["python3", script_name])
	print(script_name + " executed")

for el in os.listdir(os.curdir):
	if os.path.isfile(el):
		if el[-3:] == ".py" or el == "config.json":
			continue
		else:
			shutil.move(el, os.path.join(output_directory, el))
			print(el + " moved to " + output_directory)
			
	elif os.path.isdir(el):
		if el == "template" or el == "settings" or el == "lab_ctf":
			continue
		else:
			shutil.move(el, os.path.join(output_directory, el))
			print(el + " moved to " + output_directory)
			
