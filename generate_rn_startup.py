import json

def generate_startup_files(template_filename):
    # Carica il template dal file
    with open(template_filename, 'r') as template_file:
        template = template_file.read()

    # Carica i dati dal file JSON di input
    with open('config.json', 'r') as f:
        data = json.load(f)
        teams = data['Teams']

    # Itera attraverso i team e crea un file di startup per ciascuno
    for i, team in enumerate(teams, start=1):
        team_id = i
        startup_content = template.replace('<id_team>', str(team_id))
        startup_content = startup_content.replace("<1+id_team>", str(1 + team_id))

        # Nome del file di output basato su id_team
        output_filename = f'r{team_id}.startup'

        # Scrivi il contenuto nel file di output nella directory corrente
        with open(output_filename, 'w') as output_file:
            output_file.write(startup_content)

# Chiama la funzione generate_startup_files per generare i file di startup nella directory corrente
generate_startup_files('template/lab_template/rn.startup')
