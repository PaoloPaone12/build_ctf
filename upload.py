import json

# Leggi il file competition_info.json
with open('/fixtures/competition_info.json', 'r') as competition_file:
    competition_info = json.load(competition_file)

# Crea voci per la tabella auth.user e registration.team
user_entries = []
team_entries = []

for i in range(competition_info['num_teams']):
    username = f"Team{i + 1}"
    team_entry = {
        'model': 'registration.team',
        'pk': i + 2,  # Incrementa il pk in modo univoco
        'fields': {
            'net_number': i + 1,
            'informal_email': f"{username.lower()}@example.org",
            'image': "",
            'affiliation': "",
            'country': "IT",
            'nop_team': False,
        }
    }

    user_entry = {
        'model': 'auth.user',
        'pk': i + 2,  # Incrementa il pk in modo univoco
        'fields': {
            'password': "pbkdf2_sha256$36000$kHAF2GkRGCyG$qm+7EyJr0b8E9VbQWp3ZtfxaV0A5wIJSV/ABWEML6II=",
            'last_login': None,
            'is_superuser': False,
            'username': username,
            'first_name': username,
            'last_name': username,
            'email': "",
            'is_staff': False,
            'is_active': True,
            'date_joined': competition_info['Time_services_public'],
            'groups': [],
            'user_permissions': [],
        }
    }

    team_entries.append(team_entry)
    user_entries.append(user_entry)

# Crea la voce per la tabella scoring.gamecontrol una sola volta
gamecontrol_entry = {
    'model': 'scoring.gamecontrol',
    'pk': 1,
    'fields': {
        'competition_name': competition_info['service_name'],
        'tick_duration': 180,
        'valid_ticks': 5,
        'current_tick': -1,
        'flag_prefix': "U29tZSBzdHJpbmcgZXhh",
        'registration_open': False,
        'services_public': competition_info['Time_services_public'],
        'start': competition_info['Time_start'],
        'end': competition_info['Time_end'],
    }
}

# Crea voci per la tabella scoring.service
service_entries = []
for i, service_info in enumerate(competition_info['services']):
    service_entry = {
        'model': 'scoring.service',
        'pk': i + 1,  # Incrementa il pk in modo univoco
        'fields': {
            'name': service_info['service_name'],
            'slug': service_info['service_slug'],
        }
    }
    service_entries.append(service_entry)

# Aggiungi le nuove voci per auth.user, registration.team, scoring.service e scoring.gamecontrol al file fixtures_data
fixtures_data = user_entries + team_entries + service_entries + [gamecontrol_entry]

# Salva i dati aggiornati in un nuovo file JSON
with open('/fixtures/fixtures_updated.json', 'w') as updated_file:
    json.dump(fixtures_data, updated_file, indent=4)

print("Creazione delle voci per auth.user, registration.team, scoring.service e scoring.gamecontrol completata. I dati aggiornati sono stati scritti in 'fixtures_updated.json'")
