import os
import subprocess
import json
import shutil
import yaml

# Funzione per clonare un repository Git
def clone_git_repo(repo_url, destination):
    clone_command = f'git clone {repo_url} {destination}'
    subprocess.run(clone_command, shell=True, check=True)

# Funzione per rinominare i file con la parola chiave "checker" in "mychecker.py"
def rename_checker_files(directory):
    for root, _, files in os.walk(directory):
        for file in files:
            if "checker" in file:  # Verifica se il nome del file contiene "checker"
                new_file = os.path.join(root, "mychecker.py")
                os.rename(os.path.join(root, file), new_file)
                print(f"Rinominato il file {file} in {new_file}")

# Crea la directory 'gm' se non esiste
os.makedirs('gm', exist_ok=True)

# Crea le sottodirectory 'ctf' e 'services' direttamente all'interno di 'gm' se non esistono
ctf_directory = os.path.join('gm', 'ctf')
services_directory = os.path.join('gm', 'services')
os.makedirs(ctf_directory, exist_ok=True)
os.makedirs(services_directory, exist_ok=True)

# Clona il repository Git nella directory 'gm/ctf'
clone_git_repo('https://github.com/fausecteam/ctf-gameserver.git', ctf_directory)

# Leggi il file JSON 'config.json'
with open('config.json', 'r') as json_file:
    config_data = json.load(json_file)

# Clona i servizi definiti nell'array 'Services'
for service_url in config_data.get('Services', []):
    service_name = service_url.split('/')[-1].replace('.git', '')
    service_directory = os.path.join(services_directory, service_name)
    
    # Clona il servizio Git nella directory 'gm/services/service_name'
    clone_git_repo(service_url, service_directory)

# Copia tutti i file dalla directory 'template/settings' nella directory 'settings/web'
source_directory = os.path.join('template', 'settings')
destination_directory = os.path.join('gm', 'conf', 'web')
shutil.copytree(source_directory, destination_directory)

# Verifica se la directory 'gm/conf/systemd/' esiste già e crea se non esiste
systemd_source_directory = os.path.join('settings', 'myctf', 'systemd')
systemd_destination_directory = os.path.join('gm', 'conf', 'systemd')
if not os.path.exists(systemd_destination_directory):
    os.makedirs(systemd_destination_directory)

# Copia tutti i file dalla directory 'settings/myctf/systemd/' nella directory 'gm/conf/systemd/'
for item in os.listdir(systemd_source_directory):
    source_item = os.path.join(systemd_source_directory, item)
    destination_item = os.path.join(systemd_destination_directory, item)
    if os.path.isdir(source_item):
        shutil.copytree(source_item, destination_item)
    else:
        shutil.copy2(source_item, destination_item)

# Verifica se la directory 'gm/conf/components/' esiste già e crea se non esiste
components_source_directory = os.path.join('settings', 'myctf', 'env')
components_destination_directory = os.path.join('gm', 'conf', 'components')
if not os.path.exists(components_destination_directory):
    os.makedirs(components_destination_directory)

# Copia tutti i file dalla directory 'settings/my_ctf/env/' nella directory 'gm/conf/components/'
for item in os.listdir(components_source_directory):
    source_item = os.path.join(components_source_directory, item)
    destination_item = os.path.join(components_destination_directory, item)
    if os.path.isdir(source_item):
        shutil.copytree(source_item, destination_item)
    else:
        shutil.copy2(source_item, destination_item)

print("Copia dei file di ambiente completata con successo.")

# Copia tutti i file dalla directory /services/<nome_servizio>/checker/ nella directory /conf/checker/scripts/<nome_servizio>/
for service_directory in os.listdir(services_directory):
    checker_directory = os.path.join(services_directory, service_directory, 'checker')
    if os.path.exists(checker_directory):
        destination_directory = os.path.join('gm', 'conf', 'checker', 'scripts', service_directory)
        shutil.copytree(checker_directory, destination_directory)
        print(f"Copiati i file checker per {service_directory} in {destination_directory}")
        # Rinomina i file con la parola chiave "checker" in "mychecker.py"
        rename_checker_files(destination_directory)

# Aggiungi questa parte per creare il percorso di output se non esiste
for root, _, files in os.walk(services_directory):
    for file in files:
        if file == 'metadata.yml':
            metadata_file = os.path.join(root, file)
            with open(metadata_file, 'r') as yaml_file:
                metadata = yaml.safe_load(yaml_file)
                if 'name' in metadata and 'slug' in metadata:
                    service_slug = metadata['slug']
                    service_name = os.path.basename(os.path.dirname(metadata_file))
                    env_template_path = os.path.join('settings','myctf', 'env', 'service_pattern.env')
                    env_output_directory = os.path.join('gm','conf', 'checker', 'env')
                    os.makedirs(env_output_directory, exist_ok=True)  # Crea la directory di output se non esiste
                    env_output_path = os.path.join(env_output_directory, f'{service_slug}.env')
                    
                    # Leggi il modello .env
                    with open(env_template_path, 'r') as env_template_file:
                        env_template = env_template_file.read()
                    
                    # Sostituisci i placeholder con i valori del servizio
                    env_template = env_template.replace('"${name}"', f'{service_name}')
                    env_template = env_template.replace('"${slug}"', f'"{service_slug}"')
                    # Sostituisci i placeholder con i valori del servizio
                    # Scrivi il file .env aggiornato
                    with open(env_output_path, 'w') as env_output_file:
                        env_output_file.write(env_template)
                    
                    print(f"Sostituiti placeholder per {service_name} ({service_slug}).")

# Definisci i percorsi dei file da copiare
script_paths = ['build_info.py', 'upload.py']
config_json_path = 'config.json'

# Crea la directory 'gm/fixtures' se non esiste
fixtures_directory = os.path.join('gm', 'fixtures')
os.makedirs(fixtures_directory, exist_ok=True)

for script_path in script_paths:
    # Verifica se il file script esiste
    if os.path.isfile(script_path):
        # Copia il file script nella directory 'gm/fixtures'
        shutil.copy(script_path, fixtures_directory)
        print(f"Copiato il file {script_path} in {fixtures_directory}")
    else:
        print(f"Il file {script_path} non esiste.")

# Verifica se il file 'config.json' esiste
if os.path.isfile(config_json_path):
    # Copia il file 'config.json' nella directory 'gm/fixtures'
    shutil.copy(config_json_path, fixtures_directory)
    print(f"Copiato il file {config_json_path} in {fixtures_directory}")

else:
    print(f"Il file {config_json_path} non esiste.")

print("Operazioni completate con successo.")
