import json

def replace_placeholders(template, team_id, pc_id):
    # Sostituisci i segnaposto con i valori opportuni
    template = template.replace("<id_team>", str(team_id))
    template = template.replace("<id_pc>", str(pc_id))
    
    # Aggiungi la logica per generare l'indirizzo IP della rotta
    ip_address = f"10.80.{team_id}.{pc_id}"
    template = template.replace("<ip_address>", ip_address)

    return template

def generate_startup_scripts(config_filename, template_filename):
    # Carica i dati dal file JSON
    with open(config_filename, 'r') as config_file:
        data = json.load(config_file)
        teams = data['Teams']

    # Leggi il template dal file
    with open(template_filename, 'r') as template_file:
        template = template_file.read()

    # Genera e scrivi i file di startup per ciascun team e PC
    for team_id, _ in enumerate(teams, start=1):
        for pc_id in range(1, 3):  # Genera per pc1 e pc2
            startup_content = replace_placeholders(template, team_id, pc_id)
            filename = f"pc{team_id}_{pc_id}.startup"
            with open(filename, 'w') as startup_file:
                startup_file.write(startup_content)

# Chiama la funzione per generare i file di startup
generate_startup_scripts('config.json', 'template/lab_template/pcn.startup')
