import json
import subprocess
import os

# Nome del file di configurazione
config_file = 'config.json'

# Nome della directory di destinazione
destination_directory = 'vuln_service'

# Leggi il file di configurazione JSON
try:
    with open(config_file, 'r') as f:
        config_data = json.load(f)
except FileNotFoundError:
    print(f"File di configurazione '{config_file}' non trovato.")
    exit(1)
except json.JSONDecodeError:
    print(f"Errore nella decodifica del file di configurazione '{config_file}'. Assicurati che sia un file JSON valido.")
    exit(1)

# Crea la directory di destinazione se non esiste
if not os.path.exists(destination_directory):
    os.makedirs(destination_directory)

# Estrai gli URL dei servizi
services = config_data.get('Services', [])

# Clona le repository sotto la directory di destinazione
for service_url in services:
    # Estrai il nome del repository dalla URL
    repo_name = service_url.split('/')[-1].replace('.git', '')

    # Crea il percorso completo per la nuova directory
    service_directory = os.path.join(destination_directory, repo_name)

    # Clona la repository
    try:
        subprocess.check_output(['git', 'clone', service_url, service_directory])
        print(f"Clonata la repository '{repo_name}' sotto '{service_directory}'")
    except subprocess.CalledProcessError as e:
        print(f"Errore durante la clonazione della repository '{repo_name}': {e}")
