import os
import shutil

# Crea la directory "db" se non esiste
db_directory = 'db'
if not os.path.exists(db_directory):
    os.makedirs(db_directory)

# Crea la subdirectory "patch" all'interno di "db" se non esiste
patch_directory = os.path.join(db_directory, 'patch')
if not os.path.exists(patch_directory):
    os.makedirs(patch_directory)

# Specifica il percorso completo del file "scoring.sql" nella directory "settings"
source_file = 'settings/web/scoring.sql'

# Specifica il percorso completo di destinazione nella subdirectory "patch" di "db"
destination_file = os.path.join(patch_directory, 'scoring.sql')

# Copia il file dalla directory "settings" alla subdirectory "patch" di "db"
shutil.copy(source_file, destination_file)

print(f'Il file {source_file} è stato copiato in {destination_file}')
