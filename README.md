# Laboratorio CTF

## Accesso al Servizio

Per accedere al servizio, seguire attentamente queste istruzioni:

1. **Configurazione dei Parametri di Configurazione**:

   - Aprire il file "config.json" nella directory del progetto.
   - Modificare i parametri di configurazione in base alle proprie esigenze. Assicurarsi di impostare correttamente tutte le opzioni necessarie.

2. **Compilazione del Laboratorio**:

   - Eseguire l'istruzione seguente per compilare il laboratorio:

   ```bash
   python3 configurator.py

3. **Avvio laboratorio Kathara**:
   - Eseguire l'istruzione seguente per avviare il laboratorio:

   ```bash
   sudo kathara lstart -d lab_ctf --privileged
