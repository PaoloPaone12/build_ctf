import json
import os

def clone_github_repos(data):
    services = data['Services']
    teams = data['Teams']

    for team in teams:
        team_name = team['Team_name']
        team_directory = f'{team_name}/opt/services'
        os.makedirs(team_directory, exist_ok=True)

        for service_url in services:
            service_name = service_url.split('/')[-1].replace('.git', '')
            service_directory = f'{team_directory}/{service_name}'

            # Clona il repository GitHub nella directory del servizio
            os.system(f'git clone {service_url} {service_directory}')

if __name__ == "__main__":
    with open('config.json', 'r') as f:
        data = json.load(f)
        clone_github_repos(data)
