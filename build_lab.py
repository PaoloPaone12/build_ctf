import json
import string

def build_lab(filename):
    # Carica i dati dal file JSON
    with open(filename, 'r') as f:
        data = json.load(f)
        teams = data['Teams']

    # Crea una lista di domini di collisione disponibili (tutte le lettere dell'alfabeto tranne 'B')
    available_collision_domains = [char for char in string.ascii_uppercase if char != 'B']
    num_teams = len(teams)

    # Inizializza il contenuto del nuovo file 'lab.conf'
    lab_conf_content = f"LAB_NAME={data['Name_Competition']}\n"

    # Aggiungi informazioni specifiche per ciascun team
    for i, team in enumerate(teams, start=1):
        lab_conf_content += f"#Team{i}\n"
        lab_conf_content += f"pc{i}_1[0]={available_collision_domains[i - 1]}\n"
        lab_conf_content += f"pc{i}_2[0]={available_collision_domains[i - 1]}\n"
        lab_conf_content += f"r{i}[0]={available_collision_domains[i - 1]}\n"
        lab_conf_content += f"r{i}[1]=B\n\n"

    # NopTeam
    lab_conf_content += "#NopTeam\n"
    lab_conf_content += f"nt[0]={available_collision_domains[num_teams]}\n"
    lab_conf_content += f"nt[bridged]=\"true\"\n"
    lab_conf_content += f"nt[image]=\"kathara/dind\"\n\n"

    # GameSystem
    lab_conf_content += "#GameSystem\n"
    lab_conf_content += f"gm[0]={available_collision_domains[num_teams + 1]}\n"
    lab_conf_content += f"gm[1]={available_collision_domains[num_teams * 2 + 2]}\n"
    lab_conf_content += "gm[bridged]=\"true\"\n"
    lab_conf_content += "gm[image]=\"kathara/base\"\n\n"

    # VMs per ciascun team
    for i, team in enumerate(teams, start=1):
        lab_conf_content += f"#{team['Team_name']}\n"
        lab_conf_content += f"{team['Team_name']}[0]={available_collision_domains[num_teams + i + 1]}\n"
        lab_conf_content += f"{team['Team_name']}[image]=\"kathara/dind\"\n"
        lab_conf_content += f"{team['Team_name']}[bridged]=true\n\n"

    # MainRouter
    lab_conf_content += "#MainRouter\n"
    lab_conf_content += "mr[0]=B\n"
    lab_conf_content += f"mr[1]={available_collision_domains[num_teams + 1]}\n"
    lab_conf_content += f"mr[2]={available_collision_domains[num_teams]}\n"

    for i, team in enumerate(teams, start=1):
        next_letter = available_collision_domains[num_teams + i + 1]
        lab_conf_content += f"mr[{2 + i}]={next_letter}\n"

    # DB
    lab_conf_content += "#DB\n"
    lab_conf_content += f"db[0]={available_collision_domains[num_teams * 2 + 2]}\n"
    lab_conf_content += "db[bridged]=\"true\"\n"
    lab_conf_content += "db[image]=\"kathara/base\"\n"

    # Scrivi il nuovo file 'lab.conf'
    with open('lab.conf', 'w') as output_file:
        output_file.write(lab_conf_content)

# Chiama la funzione build_lab con il file JSON 'config.json' come argomento
build_lab('config.json')
