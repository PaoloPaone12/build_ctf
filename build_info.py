import os
import json
import yaml

# Percorso alla directory contenente i servizi clonati
service_directory = '/services'

# Percorso al file di configurazione
config_file = '/fixtures/config.json'

# Percorso del file JSON di output
output_file = '/fixtures/competition_info.json'

# Inizializza un dizionario per raccogliere le informazioni sulla competizione
competition_info = {}

# Leggi il file di configurazione JSON
try:
    with open(config_file, 'r') as f:
        config_data = json.load(f)
except FileNotFoundError:
    print(f"File di configurazione '{config_file}' non trovato.")
    exit(1)
except json.JSONDecodeError:
    print(f"Errore nella decodifica del file di configurazione '{config_file}'. Assicurati che sia un file JSON valido.")
    exit(1)

# Estrai i valori "Name_Competition", "Time_services_public", "Time_start" e "Time_end" dal file di configurazione
competition_info['service_name'] = config_data.get('Name_Competition', '')
competition_info['Time_services_public'] = config_data.get('Time_services_public', '')
competition_info['Time_start'] = config_data.get('Time_start', '')
competition_info['Time_end'] = config_data.get('Time_end', '')

# Inizializza un elenco vuoto per raccogliere le informazioni sui servizi
services_info = []

# Scansiona la directory dei servizi
for service_name in os.listdir(service_directory):
    service_path = os.path.join(service_directory, service_name)
    if os.path.isdir(service_path):
        metadata_file = os.path.join(service_path, 'metadata.yml')

        # Leggi il file metadata.yml
        try:
            with open(metadata_file, 'r') as f:
                metadata_data = yaml.safe_load(f)
                service_info = {
                    'service_name': metadata_data.get('name', ''),
                    'service_slug': metadata_data.get('slug', ''),
                }
                services_info.append(service_info)
        except FileNotFoundError:
            print(f"File 'metadata.yml' non trovato per il servizio '{service_name}'")
        except yaml.YAMLError:
            print(f"Errore nella lettura del file 'metadata.yml' per il servizio '{service_name}'")

# Aggiungi le informazioni sui servizi al dizionario delle informazioni sulla competizione
competition_info['services'] = services_info

# Aggiungi il numero di squadre al dizionario delle informazioni sulla competizione
num_teams = len(config_data.get('Teams', []))
competition_info['num_teams'] = num_teams

# Salva le informazioni sulla competizione in un file JSON
with open(output_file, 'w') as f:
    json.dump(competition_info, f, indent=2)

print(f"Le informazioni sulla competizione sono state scritte in '{output_file}'")
