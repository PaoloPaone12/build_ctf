# Nome del file template
template_file = "template/lab_template/gm.startup"

# Leggi il contenuto del file template
with open(template_file, "r") as template:
    template_content = template.read()

# Nome del file di destinazione
output_file = "gm.startup"

# Scrivi il contenuto del template nello script di output
with open(output_file, "w") as file:
    file.write(template_content)

print(f"Creato lo script {output_file}")
