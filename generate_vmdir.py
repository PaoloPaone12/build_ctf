import json
import os

def generate_team_scripts(data):
    teams = data['Teams']

    for i, team in enumerate(teams, start=1):
        team_name = team['Team_name']
        team_script_filename = f'{team_name}.startup'

        with open(team_script_filename, 'w') as team_script:
            team_script.write(f'#!/bin/bash\n\n')
            team_script.write(f'ifconfig eth0 10.60.{i}.1/24 up\n')
            team_script.write(f'route add -net 10.80.0.0/16 gw 10.60.{i}.2 dev eth0\n')
            team_script.write(f'route add -net 10.10.0.0/30 gw 10.60.{i}.2 dev eth0\n')
            team_script.write(f'route add -net 10.254.0.0/24 gw 10.60.{i}.2 dev eth0\n')
            team_script.write(f'route add -net 10.60.0.0/16 gw 10.60.{i}.2 dev eth0\n')
            team_script.write(f'echo \'nameserver 8.8.8.8\' >> /etc/resolv.conf\n')
            team_script.write(f'apt install -y apt-transport-https ca-certificates curl software-properties-common\n')
            team_script.write(f'curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose\n')
            team_script.write(f'chmod +x /usr/local/bin/docker-compose\n')

        os.chmod(team_script_filename, 0o755)

    # Genera il file di configurazione per nt.startup con id_team uguale a 0
    nt_script_filename = 'nt.startup'
    with open(nt_script_filename, 'w') as nt_script:
        nt_script.write(f'#!/bin/bash\n\n')
        nt_script.write(f'ifconfig eth0 10.60.0.1/24 up\n')
        nt_script.write(f'route add -net 10.80.0.0/16 gw 10.60.0.2 dev eth0\n')
        nt_script.write(f'route add -net 10.10.0.0/30 gw 10.60.0.2 dev eth0\n')
        nt_script.write(f'route add -net 10.254.0.0/24 gw 10.60.0.2 dev eth0\n')
        nt_script.write(f'route add -net 10.60.0.0/16 gw 10.60.0.2 dev eth0\n')
        nt_script.write(f'echo \'nameserver 8.8.8.8\' >> /etc/resolv.conf\n')
        nt_script.write(f'apt install -y apt-transport-https ca-certificates curl software-properties-common\n')
        nt_script.write(f'curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose\n')
        nt_script.write(f'chmod +x /usr/local/bin/docker-compose\n')

    os.chmod(nt_script_filename, 0o755)

if __name__ == "__main__":
    with open('config.json', 'r') as f:
        data = json.load(f)
        generate_team_scripts(data)
