import json


def generate_mr_startup(input_filename, template_filename, output_filename):
    # Carica i dati dal file JSON di input
    with open(input_filename, 'r') as f:
        data = json.load(f)
        teams = data['Teams']
        
    # Inizializza le righe del nuovo file
    new_content = []

    # Leggi il template dal file
    with open(template_filename, 'r') as template_file:
        template_lines = template_file.readlines()
        
    # Itera su ogni riga de file e sostituisce i placeholder
    for line in template_lines:
        if "id_team" in line and not line.startswith("#"):
            for i, team in enumerate(teams, start=1):
                start_line = line
                team_id = i
                start_line = start_line.replace("<id_team>", str(team_id))
                start_line = start_line.replace("<1+id_team>", str(1 + team_id))
                start_line = start_line.replace("<2+id_team>", str(2 + team_id))
                new_content.append(start_line)
        else:
            new_content.append(line)
    
    # Scrivi il contenuto del file di startup
    with open(output_filename, 'w') as f:
        for line in new_content:
            f.write(line)
                

# Chiama la funzione generate_mr_startup con il file JSON di input e il nome del file di output desiderato
generate_mr_startup('config.json','template/lab_template/mr.startup', 'mr.startup')