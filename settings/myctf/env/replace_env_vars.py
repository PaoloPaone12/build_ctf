import os

# Elenco dei file in cui vuoi sostituire i placeholder
files_to_update = ["controller.env", "checkermaster.env", "submission.env"]

# Apri il file db.env in modalità lettura
with open("db.env", "r") as env_file:
    for line in env_file:
        # Dividi la linea in chiave e valore utilizzando il carattere "=" come separatore
        key, value = line.strip().split("=")
        # Rimuovi eventuali virgolette intorno al valore
        value = value.strip('"')
        
        # Itera sui file target e sostituisci i placeholder
        for target_file in files_to_update:
            os.system(f'sed -i "s|\\${{{key}}}|{value}|g" {target_file}')

print("Script sovrascritto con successo.")
