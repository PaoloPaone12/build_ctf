import os
import re

# Leggi il file .env e carica le variabili d'ambiente
with open('db.env', 'r') as env_file:
    for line in env_file:
        line = line.strip()  # Rimuovi spazi bianchi e newline
        if not line:
            continue  # Ignora righe vuote
        key, value = line.split('=', 1)
        os.environ[key] = value

# Leggi il file di configurazione
with open('settings.py', 'r') as config_file:
    config_data = config_file.read()

# Trova i placeholder nel file di configurazione (es. '{{ DB_HOST }}')
placeholders = re.findall(r'{{\s*(\w+)\s*}}', config_data)

# Sostituisci i placeholder con i valori delle variabili d'ambiente
for placeholder in placeholders:
    env_value = os.environ.get(placeholder)
    if env_value is not None:
        config_data = config_data.replace(f'{{{{ {placeholder} }}}}', env_value)

# Sovrascrivi il file di configurazione con i valori sostituiti
with open('settings.py', 'w') as config_file:
    config_file.write(config_data)

print("Sostituzione completata!")
