import json
import os

def replace_placeholders(template, id_team):
    return template.replace("<id_team>", str(id_team))

def generate_team_scripts(data):
    teams = data['Teams']

    # Leggi il template per i team da un file
    with open('template/lab_template/vm.startup', 'r') as template_file:
        team_template = template_file.read()

    # Genera il file di configurazione per nt.startup con id_team uguale a 0
    nt_script_filename = 'nt.startup'
    nt_template = replace_placeholders(team_template, 0)
    with open(nt_script_filename, 'w') as nt_script:
        nt_script.write(nt_template)

    os.chmod(nt_script_filename, 0o755)

    # Genera i file di configurazione per i team
    for i, team in enumerate(teams, start=1):
        team_name = team['Team_name']
        team_script_filename = f'{team_name}.startup'
        team_script = replace_placeholders(team_template, i)  # crea una nuova variabile per ogni team

        with open(team_script_filename, 'w') as team_script_file:
            team_script_file.write(team_script)

        os.chmod(team_script_filename, 0o755)

if __name__ == "__main__":
    with open('config.json', 'r') as f:
        data = json.load(f)
        generate_team_scripts(data)
